﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Modal;

namespace DAO
{
    public class LimitsAccess
    {
        private const String nameXML = @"E:\Тп2\DAO\LimitsXML\limits.xml";
        private static XDocument xmlDocument;

        static LimitsAccess()
        {
            xmlDocument = new XDocument();
            xmlDocument = XDocument.Load(nameXML);
        }

        public static List<Limits> GetData()
        {
            List<Limits> limits = new List<Limits>();
            List<Limits> sortedLimits = new List<Limits>();            

            foreach (XElement r in xmlDocument.Element("limits").Elements("limit"))
            {
                limits.Add(new Limits(r.Attribute("nameOfCurrency").Value, Double.Parse(r.Element("limit").Value)));
            }

            //sort limits            
            sortedLimits = new List<Limits>(limits.OrderBy(l=>l.nameOfCurrency));
            return sortedLimits;
        }
        public static void Add(Limits limit)
        {
            XElement xLimit = new XElement("limit");
            xLimit.Add(new XAttribute("nameOfCurrency", limit.nameOfCurrency),                
                new XElement("limit", limit.limit));

            xmlDocument.Element("limits").Add(xLimit);

            xmlDocument.Save(nameXML);
        }
        public static void Clear()
        {
            xmlDocument.Element("limits").RemoveNodes();
            xmlDocument.Save(nameXML);
        }
        public static void AddRange(List<Limits> newLimits)
        {
            foreach (var r in newLimits)
            {
                Add(r);
            }
        }
    }
}
