﻿using Modal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public  class GetOperations
    {
        // Список Всех хронимых Операции
        public static List<Operation> GetAllOperation()
        {
            List<Operation> operation = new List<Operation>();
            List<Operation> sortedOperation;

            //sort transactions
            sortedOperation = new List<Operation>(from t in operation orderby t.dateTime select t);
            return sortedOperation;
        }
        // Список оперций клиента за месяц (будем считать что ограничение на покупку/продажу валют действуют в течении месяца)
        public static List<Operation> GetClientOperation(Client client)
        {
            List<Operation> operation = new List<Operation>();
            List<Operation> sortedOperation;

            //sort transactions
            sortedOperation = new List<Operation>(from t in operation orderby t.dateTime select t);
            return sortedOperation;
        }
    }
}
