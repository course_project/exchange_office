﻿using Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace DAO
{
    public class OperationAccess
    {
        private const String nameXML = @"E:\Тп2\DAO\OperationXML\operation.xml";
        private static XDocument xmlDocument;

        static OperationAccess()
        {
            xmlDocument = new XDocument();
            xmlDocument = XDocument.Load(nameXML);
        }

        public static List<Operation> GetData()
        {
            List<Operation> operations = new List<Operation>();
            List<Operation> sortedOperation;

            foreach (XElement t in xmlDocument.Element("transactions").Elements("operation"))
            {
                String name = t.Element("name").Value;
                String surname = t.Element("surname").Value;
                String passport = t.Element("passport").Value;
                Client client = new Client(name,surname,passport);
                String firstCurrency = t.Element("nameFromCurrency").Value;
                String secandCurrency = t.Element("nameToCurrency").Value;
                double firstAmount = Double.Parse(t.Element("amountFromCurrency").Value);
                double secAmount = Double.Parse(t.Element("amountFromCurrency").Value);
                double rate = Double.Parse(t.Element("rate").Value);
                DateTime data = DateTime.Parse(t.Element("dateTime").Value);
                Int64 operationNumber = Int64.Parse(t.Element("operationNumber").Value);
                Operation operation = new Operation(firstCurrency, secandCurrency, firstAmount, secAmount,
                     rate, data, operationNumber, client);
                operations.Add(operation);
            }
            
            sortedOperation = new List<Operation>(from t in operations orderby t.dateTime select t);
            return sortedOperation;
        }
        public static void Add(Operation operation)
        {
            XElement xTransaction = new XElement("operation");
            xTransaction.Add( 
                new XElement("nameFromCurrency", operation.fromCurrency),
                new XElement("nameToCurrency", operation.toCurrency),
                new XElement("amountFromCurrency", operation.amountFromCurrency.ToString()),
                new XElement("rate", operation.rate.ToString()),
                new XElement("amountToCurrency", operation.amountToCurrency.ToString()),
                new XElement("dateTime", operation.dateTime.ToString()),
                new XElement("operationNumber", operation.operationNumber.ToString()),
                new XElement("name", operation.client.Name.ToString()),
                new XElement("surname", operation.client.Surname.ToString()),
                new XElement("passport", operation.client.Passport.ToString())
                );

            xmlDocument.Element("transactions").Add(xTransaction);

            xmlDocument.Save(nameXML);
        }
    }
}
