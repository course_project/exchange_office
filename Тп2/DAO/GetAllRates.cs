﻿using Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class GetAllRates
    {
        public static List<Rate> GetData()
        {
            List<Rate> rates = new List<Rate>();
            List<Rate> sortedRate;

            //sort 
            sortedRate= new List<Rate>(from t in rates orderby t.Currency select t);
            return sortedRate;
        }
    }
}
