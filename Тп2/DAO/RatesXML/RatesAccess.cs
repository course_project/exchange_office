﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modal;
using System.Xml;
using System.Xml.Linq;


namespace DAO
{
    public class RatesAccess
    {
        private const String nameXML = @"E:\Тп2\DAO\RatesXML\rates.xml";
        private static XDocument xmlDocument;

        static RatesAccess()
        {
            xmlDocument = new XDocument();
            xmlDocument =XDocument.Load(nameXML);
        }

        public static List<Rate> GetData()
        {
            List<Rate> rates = new List<Rate>();
            List<Rate> sortedRates = new List<Rate>();

            foreach (XElement r in xmlDocument.Element("rates").Elements("rate"))
            {
                rates.Add( new Rate(r.Attribute("nameFromCurrency").Value, r.Attribute("nameToCurrency").Value, 
                    Double.Parse(r.Element("rateFrom").Value), Double.Parse(r.Element("rateTo").Value), DateTime.Parse(r.Element("dateTime").Value)));
            }
            
            List<Rate> ratesBYN = new List<Rate>(from r in rates where r.nameFromCurrency == "BYN" select r);
            ratesBYN.AddRange(from r in rates where r.nameToCurrency == "BYN" select r);
            sortedRates = new List<Rate>(from t in rates orderby t.dateTime select t);
            return sortedRates;
        }
        public static void Add(Rate rate)
        {
            XElement xRate = new XElement("rate");
            xRate.Add(new XAttribute("nameFromCurrency",rate.nameFromCurrency), 
                new XAttribute("nameToCurrency",rate.nameToCurrency), 
                new XElement("rateFrom",rate.rateFrom.ToString()), 
                new XElement("rateTo",rate.rateTo.ToString()),
                new XElement("dateTime", rate.dateTime.ToString()));

            xmlDocument.Element("rates").Add(xRate);

            xmlDocument.Save(nameXML);
        }
        public static void Clear()
        {
            xmlDocument.Element("rates").RemoveNodes();
            xmlDocument.Save(nameXML);
        }
        public static void AddRange(List<Rate> newRates)
        {
            foreach(var r in newRates)
            {
                Add(r);
            }
        }
    }
}
