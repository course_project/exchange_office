﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modal;
using System.Xml.Linq;

namespace DAO
{
    public class ClientAccess
    {
        private const String nameXML = @"D:\1 университет\5 семестр\Прога\Проект\ExchangeOffice\DataBaseAccess\UsersXML\users.xml";
        private static XDocument xmlDocument;

        static ClientAccess()
        {
            xmlDocument = new XDocument();
            xmlDocument = XDocument.Load(nameXML);
        }

        public static List<Client> GetData()
        {
            List<Client> client = new List<Client>();
            List<Client> sortedClient;

            foreach (XElement u in xmlDocument.Element("users").Elements("user"))
            {
                client.Add(new Client(u.Element("name").Value, u.Element("surname").Value, u.Attribute("id").Value));
            }
            
            sortedClient = new List<Client>(client.OrderBy(u=>u.Surname));
            return sortedClient;
        }
        public static void Add(Client user)
        {
            XElement xUser = new XElement("user");
            xUser.Add(new XAttribute("id", user.Passport),
                new XElement("name", user.Surname),
                new XElement("surname", user.Surname));

            xmlDocument.Element("users").Add(xUser);

            xmlDocument.Save(nameXML);
        }
    }
}
