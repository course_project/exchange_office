﻿using System;
namespace Service
{
    public class Client
    {
        private string name;
        private string passport;
        private string surname;

        public Client() 
        {
        }

        public Client(string name, string surname, string passport)
        {
            this.name = name;
            this.surname = surname;
            this.passport = passport;
        }
        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                this.name = Name;
            }
        }
        public String Surname
        {
            get
            {
                return surname;
            }
            set
            {
                this.surname = Surname;
            }
        }
        public String Passport
        {
            get
            {
                return passport;
            }
            set
            {
                this.passport = Passport;
            }
        }
    }
}