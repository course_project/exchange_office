﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modal;
using DAO;

namespace Service
{
    public static class LimitValidator
    {
        public static Boolean ValidateLimit(Double amountFrom, Double amountTo, Double commonFrom, Double commonTo,  Double limitFrom, Double limitTo)
        {
            if (((amountFrom + commonFrom) > limitFrom) || ((amountFrom + commonTo) > limitTo))
            {
                return false;
            }
            else
            {
                return true;
            }
        }        
    }
}
