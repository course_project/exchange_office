﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modal;
using System.IO;

namespace Service
{
    public static class CheckPrinter
    {
        private static Int64 operationNumber;       
        private const String path = @"E:\Тп2\Service\bill{0}.doc";
       
        public static void PrintCheck(Client client, Operation operation)
        {
            operationNumber= operation.operationNumber;
            using (StreamWriter sw = new StreamWriter(String.Format(path, operationNumber), false, System.Text.Encoding.Default))
            {
                sw.WriteLine();
                sw.WriteLine("Name: {0}", client.Name);
                sw.WriteLine("Surname: {0}", client.Surname);
                sw.WriteLine("Date : {0}", operation.dateTime.Date);
                sw.WriteLine("Time : {0}", operation.dateTime.TimeOfDay);
                sw.WriteLine("Currency {0} exchanged from {0} amaunt {1} to {2} acount{3} on rates {4}",
                    operation.fromCurrency, operation.amountFromCurrency, operation.toCurrency, operation.amountToCurrency, operation.rate);
                sw.WriteLine("Operation Number: {0}", operationNumber);
                sw.WriteLine();
            }
        }
    
        public static String GetFileName()
        {
            return String.Format(path, operationNumber);
        }        
    }
}
