﻿using System;
using Modal;
namespace Service
{
    public static class Exchange
    {
        private static Client client_;
        private static Client operation_;

        public static Client Client { get; set; }
        public static Operation Operation { get; set; }

        public static double CalculateAmount(double rate, double amountFromCurrency)
        {
            return amountFromCurrency * rate;
        }
    }
}
