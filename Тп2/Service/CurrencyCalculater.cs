﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modal;

namespace Service
{
    class CurrencyCalculater
    {
        public static void CalculateRate(Rate rate, bool operationFlag)
        {
            if (operationFlag)
            {
                 rate.rateFrom = rate.rateTo * 0.9;
            }
            else
            {                        
                rate.rateTo = rate.rateFrom * 1.10;
            }
        }
    }
}
