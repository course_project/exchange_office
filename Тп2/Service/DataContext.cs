﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modal;


namespace Service
{
    public class DataContext
    {
        public RateRepository rates { get; private set; }
        public OperationRepository operations { get; private set; }
        public LimitRepository limits { get; private set; }

        public DataContext()
        {
            rates = new RateRepository();
            operations = new OperationRepository();
            limits = new LimitRepository();
        }
      
        //RATES
        public void Add(Rate newRate, Boolean CalculateRate, bool operationFlag)
        {
            if (newRate.nameFromCurrency == newRate.nameToCurrency) throw new FormatException();
            if (!CalculateRate)
            {
                if (newRate.rateTo <= 0 || newRate.rateFrom <= 0) throw new FormatException();
            }
            else
            {
                if ((newRate.rateTo <= 0 && newRate.rateTo != -1) || (newRate.rateFrom <= 0 && newRate.rateFrom != -1)) throw new FormatException();
                CurrencyCalculater.CalculateRate(newRate, operationFlag);

            }
            
            if(rates.Contain(newRate.nameFromCurrency, newRate.nameToCurrency))
            {
                throw new ExchangeRateAlreadyExistException();
            }
            
            rates.Add(newRate);
        }
        public void Add(Rate newRate, int index, Boolean CalculateRate, bool operationFlag)
        {
            if (!CalculateRate)
            {
                if (newRate.rateTo <= 0 || newRate.rateFrom <= 0) throw new FormatException();
            }
            else
            {
                if ((newRate.rateTo <= 0 && newRate.rateTo != -1) || (newRate.rateFrom <= 0 && newRate.rateFrom != -1)) throw new FormatException();
                CurrencyCalculater.CalculateRate(newRate, operationFlag);
            }
           
            rates.Add(newRate, index);
        }

        //TRANSACTIONS
        public void Add(Operation newOperation)
        {
            try
            {
                if (!LimitValidator.ValidateLimit(newOperation.amountFromCurrency, newOperation.amountToCurrency, 
                    operations.CalculateVolumeFrom(newOperation.fromCurrency), operations.CalculateVolumeFrom(newOperation.toCurrency), 
                    limits.Find(newOperation.fromCurrency), limits.Find(newOperation.toCurrency)))
                {
                    throw new LimitExcessExeption();
                }
            }
            catch (LimitNotFoundException) { throw new LimitNotFoundException(); }
            operations.Add(newOperation);
        }

        //USERS
       /* public void Add(Client newClient)
        {
            if(users.Contain(newClient.Passport))
            {
            }
            try
            {
                if (IsRightFormat(newClient.Name) && IsRightFormat(newClient.Surname)) users.Add(newClient);
            }
            catch (FormatException) { throw new FormatException(); }
            
        }*/

        //LIMITS
       /* public void Set(Limits limit)
        {
            if (limit.limit <= 0) throw new FormatException();
            Boolean isCurExist=false;
            foreach(var c in rates.GetCurrencies())
            {
                if (c.Equals(limit.nameOfCurrency))
                {
                    isCurExist = true;
                    break;
                }
            }
            if (!isCurExist) throw new LimitNotFoundException();
            limits.Add(limit);

        }*/

        /*public Boolean IsRightFormat(String name)
        {
            foreach(var se in name)
            {
                if (!((se >= 'a' && se <= 'z') || (se >= 'A' && se <= 'Z')))
                {
                    foreach (var sr in name)
                    {
                        if (!((sr >= 'а' && sr <= 'я') || (sr >= 'А' && sr <= 'Я'))) throw new FormatException();
                        
                    }
                    return true;                     
                            
                }
                    
            }
            return true;
        }
       /* public void SavChangese(List<Int32> deleteRows)
        {
            rates.Save(deleteRows);
        }

        public List<String> GetCurrencies()
        {
            return rates.GetCurrencies();
        }
        */
    }
}
