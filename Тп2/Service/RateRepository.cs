﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modal;
using DAO;
using System.Collections;

namespace Service
{
    public class RateRepository: IEnumerable
    {
        private List<Rate> rates { get; set; }
    
        private class InitialRate
        {
            public Rate rate;
            public int position;

            public InitialRate(Rate rate, int position)
            {
                this.rate = rate;
                this.position = position;
            }
        }
        private Stack<InitialRate> stack;

        internal RateRepository()
        {
            rates = RatesAccess.GetData();
            stack = new Stack<InitialRate>();
        }

        public void Update()
        {
            rates = RatesAccess.GetData();
        }

        public Double Find(String firstCurrency, String secandCurrency)
        {
            Update();
            foreach (var r in rates)
            {
                if (r.nameFromCurrency.Equals(firstCurrency) && r.nameToCurrency.Equals(secandCurrency))
                {
                    return r.rateFrom;
                }
                else
                {
                    if (r.nameFromCurrency.Equals(secandCurrency) && r.nameToCurrency.Equals(firstCurrency))
                    {
                        return r.rateTo;
                    }
                }
            }
            throw new ExchangeRateNotFoundException();
        }
       
        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable<Rate>)rates).GetEnumerator();
        }

        public Rate this[int index]
        {
            get
            {
                if (index < rates.Count)
                {
                    return rates[index];
                }
                else
                {
                    return null;
                }
            }
           set
            {
                if (index < rates.Count)
                {
                    rates[index] = value;
                }
            }
        }
        public void Save(List<Int32> deleteRows)
        {
            RatesAccess.Clear();
            Int32 n = 0;
            foreach(var r in rates)
            {
                if (!deleteRows.Contains(n++))
                {

                    RatesAccess.Add(r);
                }
            }
            
        }
        internal void Add(Rate r)
        {
            rates.Add(r);
        }
        internal void Add(Rate r, int index)
        {
            rates[index] = r;
        }
        public List<String> GetCurrencies()
        {
            List<String> currencies = new List<String>();
            foreach(var r in rates)
            {
                if (currencies == null)
                {
                    currencies.Add(r.nameToCurrency);
                    currencies.Add(r.nameFromCurrency);
                }
                if (!currencies.Contains(r.nameFromCurrency))
                {
                    currencies.Add(r.nameFromCurrency);
                }
                if (!currencies.Contains(r.nameToCurrency))
                {
                    currencies.Add(r.nameToCurrency);
                }
            }
            return new List<String>(currencies.OrderBy(c => c));
        }

        public Boolean Contain(String nameF, String nameT)
        {
            Update();
            foreach(var r in rates)
            {
                if((r.nameFromCurrency==nameF && r.nameToCurrency==nameT) || (r.nameFromCurrency==nameT && r.nameToCurrency == nameF))
                {
                    return true;
                }
               
            }
            return false;
        }

        public void Push(int index)
        {
            if(index<rates.Count)
            stack.Push(new InitialRate(rates[index], index));
        }
        public void Pop()
        {
            if (stack.Count != 0)
            {
                rates[stack.Peek().position] = stack.Pop().rate;                
            }
        }

    }
}
