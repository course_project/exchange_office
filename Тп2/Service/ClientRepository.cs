﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modal;
using DAO;
using System.Collections;

namespace Service
{
    public class ClientRepository: IEnumerable
    {
        private List<Client> clients { get; set; }

        internal ClientRepository()
        {
            //clients = ClientAccess.GetData(); 
        }

        public Client Find(String passport)
        {
            Updata();
            if (clients.Any(c => c.Passport == passport))
            {
                return (from c in clients where c.Passport == passport select c).First();
            }
            else
            {
                throw new ClientNotFoundException();
            }
        }

        public Client Find(String name, String surname)
        {
            Updata();
            if ( clients.Any(c => (c.Name == name && c.Surname == surname)) ) return (from c in clients select c).First();
            return null;
        }
        public Boolean Contain(String passport)
        {
            Updata();
            if (clients.Any(u => u.Passport == passport))
            {
                return true;
            }
            return false;
        }
        internal void Add(Client client)
        {
            //ClientAccess.Add(client);
            clients.Add(client);
            Updata();
        }
        public void Updata()
        {
            //clients = ClientAccess.GetData();
        }
        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable<Client>)clients).GetEnumerator();
        }

    }
}
