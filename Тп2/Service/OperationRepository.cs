﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modal;
using DAO;
using System.Collections;

namespace Service
{
    public class OperationRepository : IEnumerable
    {
        private List<Operation> operations { get; set; }        
        
        internal OperationRepository()
        {
            operations = OperationAccess.GetData();            
        }

        //FILTERS
        public void FilterByFromCurrency(string cur)
        {
            operations = operations.FindAll(x=> x.fromCurrency == cur) ;
            
        }

        public void FilterByToCurrency(string cur)
        {
            operations = operations.FindAll(x => x.toCurrency == cur);
        }

        public void FilterByDate(DateTime from, DateTime to)
        {
            operations = operations.FindAll(x => (x.dateTime.Date >= from) && (x.dateTime.Date <= to));
            
        }
        public void FilterByClientPassport(Client client)
        {
            operations = operations.FindAll(x => x.client.Passport == client.Passport);

        }


        //CALCULATIONS
        public Double CalculateVolumeFrom(String fromCurrency)
        {
            Double amount = 0;
            foreach (var tr in operations)
            {
                if (tr.fromCurrency == fromCurrency) amount += tr.amountFromCurrency;
            }
            return amount;            
        }

        public Double CalculateVolumeTo(String toCurrency)
        {
            Double amount = 0;
            foreach (var tr in operations)
            {
                if (tr.toCurrency == toCurrency) amount += tr.amountToCurrency;
            }
            return amount;
        }

        public Double CalculateVolumeFrom()
        {
            Double amount = 0;
            foreach (var tr in operations)
            {
                amount += tr.amountFromCurrency;
            }
            return amount;
        }

        public Double CalculateVolumeTo()
        {
            Double amount = 0;
            foreach (var tr in operations)
            {
                amount += tr.amountToCurrency;
            }
            return amount;
        }

        /*public Double CalculateVolumeFrom(String fromCurrency, String id)
        {
            Double amount = 0;
            foreach (var tr in transactions)
            {
                if (tr.id == id && tr.fromCurrency == fromCurrency) amount += tr.amountFromCurrency;
            }
            return amount;

        }

        public Double CalculateVolumeTo(String toCurrency, String id)
        {
            Double amount = 0;
            foreach (var tr in transactions)
            {
                if (tr.id == id && tr.toCurrency == toCurrency) amount += tr.amountToCurrency;
            }
            return amount;
        }*/


        /*public Boolean LimitCheck(Transaction ntr, Double limitFrom, Double limitTo)
        {
            Double amount = ntr.amountFromCurrency;
            foreach (var tr in transactions)
            {
                if (tr.id == ntr.id && tr.fromCurrency==ntr.fromCurrency) amount += tr.amountFromCurrency;
            }
            if (amount >= limitFrom) return false;
            else
            {
                amount = ntr.amountToCurrency;
                foreach (var tr in transactions)
                {
                    if (tr.id == ntr.id && tr.toCurrency == ntr.toCurrency) amount += tr.amountToCurrency;
                }
                if (amount >=limitTo) return false;
                return true;
            }
                
        }*/
        public void ResetFilters()
        {
            operations = OperationAccess.GetData();
            operations = OperationAccess.GetData();
        }

        internal void Add(Operation operation)
        {
            if (operations.Count == 0)
            {
                operation.operationNumber = 1;
            }
            else operation.operationNumber = operations.Last().operationNumber + 1;
            OperationAccess.Add(operation);
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable<Operation>)operations).GetEnumerator();
        }
        
    }
}
