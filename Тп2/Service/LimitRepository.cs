﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using Modal;

namespace Service
{
    public class LimitRepository
    {
        private List<Limits> limits;

        internal LimitRepository()
        {
            limits = LimitsAccess.GetData();
        }

        public void Updata()
        {
            limits = LimitsAccess.GetData();
        }

        internal void Add(Limits limit)
        {
            if (!Contain(limit.nameOfCurrency))
            {
                limits.Add(limit);               
                LimitsAccess.Add(limit);
            }
            else
            {
                limits.RemoveAll(l=> l.nameOfCurrency == limit.nameOfCurrency);
                limits.Add(limit);
                LimitsAccess.Clear();
                LimitsAccess.AddRange(limits);
            }
            
        }

        public Double Find(String name)
        {
            Updata();
            foreach (var l in limits)
                if (l.nameOfCurrency == name) return l.limit;
            throw new LimitNotFoundException();
        }
        public Boolean Contain(String name)
        {
            Updata();
            foreach (var l in limits)
            {
                if (l.nameOfCurrency.Equals(name)) return true;
            }
                
            return false;
        }
    }
}
