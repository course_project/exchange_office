﻿using Modal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public static class  RateShow
    {
        private  const  String path = @"E:\Тп2\Service\rate.doc";

        public static void RatesShow(Double rate,String nameFromCurrency, String nameToCurrency)
        {
            using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
            {
                sw.WriteLine();
                sw.WriteLine("From currency: ", nameFromCurrency);
                sw.WriteLine("To currency:", nameToCurrency);
                sw.WriteLine("Rate", rate);
                sw.WriteLine();
            }
        }
        public static String GetFileName()
        {
            return String.Format(path);
        }

    }
}
