﻿using Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exchange_office
{
    public partial class MenuForm : Form
    {
        private DataContext data;
        Boolean newRowAdded = false;
        Boolean clearWork = false;
        List<Int32> deletedRowsNumbers;
        public MenuForm()
        {
            InitializeComponent();
            data = new DataContext();
        }
        private void MenuForm_Load(object sender, EventArgs e)
        {

        }

        private void currencyButton_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void operationButton_Click(object sender, EventArgs e)
        {
            Form1 form = new Form1();
            form.ShowDialog();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void printPreviewControl1_Click(object sender, EventArgs e)
        {
        }

        private void currencyButton_Click_1(object sender, EventArgs e)
        {
            bool saledCourencyChecked = SaledBox.CheckedItems.Count == 1;
            bool boughtCourencyChecked = SaledBox.CheckedItems.Count == 1;
            if (!(boughtCourencyChecked && saledCourencyChecked))
            {
                MessageBox.Show("Не все поля формы заполнены правильно", "ERROR", MessageBoxButtons.OK);
                return;
            }
            string saledCur = this.SaledBox.CheckedItems[0].ToString();
            string boughtCur = this.BoughtBox.CheckedItems[0].ToString();
            Double  rate = data.rates.Find(saledCur, boughtCur);
            RateShow.RatesShow(rate, saledCur,boughtCur);
            print.DocumentName = CheckPrinter.GetFileName();
            printPreviewControl1.Document = print;
            printPreviewDialog.Document = print;
        }
    }
}
