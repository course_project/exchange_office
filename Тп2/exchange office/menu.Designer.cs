﻿namespace exchange_office
{
    partial class MenuForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuForm));
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.frontLable = new System.Windows.Forms.Label();
            this.currencyButton = new System.Windows.Forms.Button();
            this.operationButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.BoughtBox = new System.Windows.Forms.CheckedListBox();
            this.SaledBox = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxHome = new System.Windows.Forms.ComboBox();
            this.printPreviewControl1 = new System.Windows.Forms.PrintPreviewControl();
            this.print = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.printDialog = new System.Windows.Forms.PrintDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
            this.pictureBox.Location = new System.Drawing.Point(21, 12);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(780, 73);
            this.pictureBox.TabIndex = 9;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // frontLable
            // 
            this.frontLable.AutoSize = true;
            this.frontLable.BackColor = System.Drawing.SystemColors.Info;
            this.frontLable.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontLable.Location = new System.Drawing.Point(511, 21);
            this.frontLable.Name = "frontLable";
            this.frontLable.Size = new System.Drawing.Size(250, 36);
            this.frontLable.TabIndex = 10;
            this.frontLable.Text = "Enter informations";
            // 
            // currencyButton
            // 
            this.currencyButton.Location = new System.Drawing.Point(90, 224);
            this.currencyButton.Name = "currencyButton";
            this.currencyButton.Size = new System.Drawing.Size(75, 23);
            this.currencyButton.TabIndex = 11;
            this.currencyButton.Text = "Currency exchange";
            this.currencyButton.UseVisualStyleBackColor = true;
            this.currencyButton.Click += new System.EventHandler(this.currencyButton_Click_1);
            // 
            // operationButton
            // 
            this.operationButton.Location = new System.Drawing.Point(704, 214);
            this.operationButton.Name = "operationButton";
            this.operationButton.Size = new System.Drawing.Size(75, 23);
            this.operationButton.TabIndex = 12;
            this.operationButton.Text = "Operation with currency";
            this.operationButton.UseVisualStyleBackColor = true;
            this.operationButton.Click += new System.EventHandler(this.operationButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(54, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(230, 33);
            this.label4.TabIndex = 13;
            this.label4.Text = "Currency exchange";
            // 
            // BoughtBox
            // 
            this.BoughtBox.FormattingEnabled = true;
            this.BoughtBox.Items.AddRange(new object[] {
            "BYN",
            "EUR",
            "USD",
            "GBP"});
            this.BoughtBox.Location = new System.Drawing.Point(21, 127);
            this.BoughtBox.Name = "BoughtBox";
            this.BoughtBox.Size = new System.Drawing.Size(66, 49);
            this.BoughtBox.TabIndex = 23;
            // 
            // SaledBox
            // 
            this.SaledBox.FormattingEnabled = true;
            this.SaledBox.Items.AddRange(new object[] {
            "BYN",
            "EUR",
            "USD",
            "GBP"});
            this.SaledBox.Location = new System.Drawing.Point(182, 127);
            this.SaledBox.Name = "SaledBox";
            this.SaledBox.Size = new System.Drawing.Size(66, 49);
            this.SaledBox.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(21, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 15);
            this.label1.TabIndex = 26;
            this.label1.Text = "Bought currency";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(182, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 15);
            this.label2.TabIndex = 27;
            this.label2.Text = "Saled currency";
            // 
            // comboBoxHome
            // 
            this.comboBoxHome.FormattingEnabled = true;
            this.comboBoxHome.Location = new System.Drawing.Point(6, 55);
            this.comboBoxHome.Name = "comboBoxHome";
            this.comboBoxHome.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHome.TabIndex = 0;
            // 
            // printPreviewControl1
            // 
            this.printPreviewControl1.Location = new System.Drawing.Point(315, 92);
            this.printPreviewControl1.Name = "printPreviewControl1";
            this.printPreviewControl1.Size = new System.Drawing.Size(250, 235);
            this.printPreviewControl1.TabIndex = 28;
            this.printPreviewControl1.Click += new System.EventHandler(this.printPreviewControl1_Click);
            // 
            // printPreviewDialog
            // 
            this.printPreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog.Enabled = true;
            this.printPreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog.Icon")));
            this.printPreviewDialog.Name = "printPreviewDialogCheck";
            this.printPreviewDialog.Visible = false;
            // 
            // printDialog
            // 
            this.printDialog.AllowSelection = true;
            this.printDialog.AllowSomePages = true;
            this.printDialog.PrintToFile = true;
            this.printDialog.ShowHelp = true;
            this.printDialog.UseEXDialog = true;
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(849, 393);
            this.Controls.Add(this.printPreviewControl1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SaledBox);
            this.Controls.Add(this.BoughtBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.operationButton);
            this.Controls.Add(this.currencyButton);
            this.Controls.Add(this.frontLable);
            this.Controls.Add(this.pictureBox);
            this.Name = "MenuForm";
            this.Text = "start";
            this.Load += new System.EventHandler(this.MenuForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label frontLable;
        private System.Windows.Forms.Button currencyButton;
        private System.Windows.Forms.Button operationButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox BoughtBox;
        private System.Windows.Forms.CheckedListBox SaledBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxHome;
        private System.Windows.Forms.PrintPreviewControl printPreviewControl1;
        private System.Drawing.Printing.PrintDocument print;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog;
        private System.Windows.Forms.PrintDialog printDialog;
    }
}

