﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Service;
using Modal;

namespace exchange_office

{
    public partial class Form2 : Form
    {
        private DataContext data = new DataContext();
        public Form2()
        {
            InitializeComponent();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.boughtCurrencyCheckedListBox.
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            DialogResult result;
            bool anyDataEnter = saledCurrencyTextBox.Text != "" || boughtCurrencyTextBox.Text != "";
            bool saledCourencyChecked = SaledCurrencyCheckedListBox.CheckedItems.Count == 1;
            bool boughtCourencyChecked = boughtCurrencyCheckedListBox.CheckedItems.Count == 1;
            if (!(boughtCourencyChecked && saledCourencyChecked && anyDataEnter))
            {
                MessageBox.Show("Не все поля формы заполнены правильно", "ERROR", MessageBoxButtons.OK);
                return;
            }
            try
            {
                string saledCur = SaledCurrencyCheckedListBox.CheckedItems[0].ToString();
                string boughtCur = boughtCurrencyCheckedListBox.CheckedItems[0].ToString();
                Double rate;
                Double saledAm;
                Double boghtAm;
                if (saledCurrencyTextBox.Text != "")
                {
                    saledAm = Convert.ToDouble(saledCurrencyTextBox.Text); 
                    rate = data.rates.Find(saledCur, boughtCur);
                    boghtAm = Exchange.CalculateAmount(rate, saledAm);
                }
                else
                {
                    boghtAm = Convert.ToDouble(saledCurrencyTextBox.Text);
                    if (boughtCurrencyTextBox.Text != "")
                    {
                        boghtAm = Convert.ToDouble(boughtCurrencyTextBox.Text);
                        rate = data.rates.Find(boughtCur, saledCur);
                        saledAm = Exchange.CalculateAmount(rate, boghtAm);
                    }
                    else
                    {
                        MessageBox.Show("Не все поля формы заполнены правильно", "ERROR", MessageBoxButtons.OK);
                        return;
                    }
                }
                Operation newOperation = new Operation(saledCur, boughtCur, saledAm,boghtAm, rate, Exchange.Client);
                result = MessageBox.Show("Подтвеждаете транзакцию?", "", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    data.Add(newOperation);
                    Close();
                    Hide();
                    Form3Succesful form = new Form3Succesful(newOperation.client,newOperation);
                    form.ShowDialog();
                }
            }
            catch (ExchangeRateNotFoundException)
            {
                MessageBox.Show("Не можем найти курс \nПроверте, задана ли данная конверсия", "Упс");
            }
            catch (LimitExcessExeption)
            {
                MessageBox.Show("К сожалению лимит превышен");
            }
            catch (FormatException)
            {
                MessageBox.Show("Проверте введённые данные", "Ошибка формата");

            }
            catch (LimitNotFoundException)
            {
                MessageBox.Show("Не можем найти лимит для одной из валют", "Упс");
            }
        }

        private void boughtCurrencyTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void SaledCurrencyCheckedListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
