﻿using System;
using Service;
using System.Windows.Forms;
using Modal;

namespace exchange_office
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void nextButton_Click(object sender, EventArgs e)
        {

            if (firstNameTextBox.Text == "" || surnameTextBox.Text == "" || patronymicTextBox.Text == "" || firstNameTextBox.Text == "")
            {
                MessageBox.Show("Не все поля формы заполнены","ERROR" , MessageBoxButtons.OK);
                return;
            }
            String name = this.firstNameTextBox.Text;
            String surname = this.surnameTextBox.Text;
            String passport = this.patronymicTextBox.Text;
            Client client = new Client(name, surname, passport);
            Exchange.Client=client;
            Close();
            Hide();
            Form2 form = new Form2();
            form.ShowDialog();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
