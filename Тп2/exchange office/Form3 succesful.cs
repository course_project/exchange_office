﻿using Modal;
using Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exchange_office
{
    public partial class Form3Succesful : Form
    {


        public Form3Succesful(Client client, Operation opreration)
        {
            InitializeComponent();
            CheckPrinter.PrintCheck(client, opreration);
            printCheck.DocumentName = CheckPrinter.GetFileName();
            printPreviewControl1.Document = printCheck;
            printPreviewDialogCheck.Document = printCheck;
        }


        private void printPreviewControl1_Click(object sender, EventArgs e)
        {

        }

        private void printPreviewDialog1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            printPreviewDialogCheck.ShowDialog();
        }

        private void printCheck_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

        }
    }
}
