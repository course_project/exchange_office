﻿using System;
namespace Modal
{
    public class Operation
    {
        public String fromCurrency { get; set; }
        public String toCurrency { get; set; }
        public Double amountFromCurrency { get; set; }
        public Double amountToCurrency { get; set; }
        public Double rate { get; set; }
        public DateTime dateTime { get; set; }
        public Int64 operationNumber { get; set; } = 0;
        public Client client { get; set; }

        public Operation(string fromCurrency, string toCurrency, double amountFromCurrency,double amountToCurrency, double rate,  Client client)
        {
            this.fromCurrency = fromCurrency;
            this.toCurrency = toCurrency;
            this.amountFromCurrency = amountFromCurrency;
            this.rate = rate;
            this.client = client;
            dateTime = DateTime.Now;
            amountToCurrency = amountFromCurrency * rate;

        }

        public Operation(string fromCurrency, string toCurrency, double amountFromCurrency, double amountToCurrency, double rate, DateTime dateTime,  Int64 operationNumber, Client client)
        {
            this.fromCurrency = fromCurrency;
            this.toCurrency = toCurrency;
            this.amountFromCurrency = amountFromCurrency;
            this.amountToCurrency = amountToCurrency;
            this.rate = rate;
            this.dateTime = dateTime;
            this.operationNumber = operationNumber;
            this.client = client;
        }
    }
}
