﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modal
{
    public class Limits
    {
        public String nameOfCurrency { get; set; }
        public Double limit { get; set; }

        public Limits(string nameOfCurrency, double limit)
        {
            this.nameOfCurrency = nameOfCurrency;
            this.limit = limit;
        }
    }
}

