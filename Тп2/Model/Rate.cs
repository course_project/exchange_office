﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modal
{
    public class Rate
    {
        private string value1;
        private string value2;
        private double v1;
        private double v2;

        public String nameFromCurrency { get; set; }
        public String nameToCurrency { get; set; }
        public Double rateTo { get; set; }
        public Double rateFrom { get; set; }
        public DateTime dateTime { get; set; }

        public Rate(string nameFromCurrency, string nameToCurrency, double rateTo, double rateFrom, DateTime dataTime)
        {
            this.nameFromCurrency = nameFromCurrency;
            this.nameToCurrency = nameToCurrency;
            this.rateTo = rateTo;
            this.rateFrom = rateFrom;
            this.dateTime = dateTime;
        }

        public Rate(string value1, string value2, double v1, double v2)
        {
            this.value1 = value1;
            this.value2 = value2;
            this.v1 = v1;
            this.v2 = v2;
        }
    }
}
