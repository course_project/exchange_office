﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class MaintainOperation
    {
        private static MaintainOperation instance;
        private Operation operation;
        private MaintainOperation(Operation operation)
        {
            this.operation = operation;
        }

        public static MaintainOperation getInstance(Operation operation)
        {
            if (instance == null)
            {
                instance = new MaintainOperation(operation);
            }
            return instance;
        }
       
    }
}
