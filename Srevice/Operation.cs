﻿using System;
namespace Service
{
    public class Operation
    {
        public Operation(Client client)
        {
            this.operationClient_ = client;
        }
        private String saledCurrency;
        private String boughtCurrency;
        private double saledCurrencyCount;
        private double boughtCurrencyCount;
        private Client operationClient_;

        public string SaledCurrency
        {
            get
            {
                return saledCurrency;
            }

            set
            {
                saledCurrency = value;
            }
        }

        public string BoughtCurrency
        {
            get
            {
                return boughtCurrency;
            }

            set
            {
                boughtCurrency = value;
            }
        }

        public double SaledCurrencyCount
        {
            get
            {
                return saledCurrencyCount;
            }

            set
            {
                saledCurrencyCount = value;
            }
        }

        public double BoughtCurrencyCount
        {
            get
            {
                return boughtCurrencyCount;
            }

            set
            {
                boughtCurrencyCount = value;
            }
        }

        public Client OperationClient
        {
            get
            {
                return operationClient_;
            }

            set
            {
                this.operationClient_ = OperationClient;
            }
        }

        public Client getOperationClient()
        {
            return this.operationClient_;
        }
        public void setOperationClient(Client client)
        {
            this.operationClient_ = client;
        }
    }
}
