﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public  static class Validator
    {
        public static bool ValidClientEnterData(Client client)
        {
            if(String.IsNullOrWhiteSpace(client.Name))
            {
                return false;
            }
            if (String.IsNullOrWhiteSpace(client.Surname))
            {
                return false;
            }
            if (String.IsNullOrWhiteSpace(client.Passport))
            {
                return false;
            }
            return true;
        }
        public static bool ValidOperationEnterData(Operation operaton)
        {
            return true;
        }
        public static bool ValidEnableOperation()
        {
            return true;
        }
    }
}
