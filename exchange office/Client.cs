﻿using System;
namespace exchange_office
{
    internal class Client
    {
        private string name;
        private string passport;
        private string surname;

        public Client(string name, string surname, string passport)
        {
            this.name = name;
            this.surname = surname;
            this.passport = passport;
        }
        public String getName()
        {
            return name;
        }
    }
}