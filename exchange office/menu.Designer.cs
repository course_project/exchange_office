﻿namespace exchange_office
{
    partial class MenuForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuForm));
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.frontLable = new System.Windows.Forms.Label();
            this.currencyButton = new System.Windows.Forms.Button();
            this.operationButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
            this.pictureBox.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(780, 73);
            this.pictureBox.TabIndex = 9;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // frontLable
            // 
            this.frontLable.AutoSize = true;
            this.frontLable.BackColor = System.Drawing.SystemColors.Info;
            this.frontLable.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontLable.Location = new System.Drawing.Point(267, 20);
            this.frontLable.Name = "frontLable";
            this.frontLable.Size = new System.Drawing.Size(250, 36);
            this.frontLable.TabIndex = 10;
            this.frontLable.Text = "Enter informations";
            // 
            // currencyButton
            // 
            this.currencyButton.Location = new System.Drawing.Point(295, 160);
            this.currencyButton.Name = "currencyButton";
            this.currencyButton.Size = new System.Drawing.Size(75, 23);
            this.currencyButton.TabIndex = 11;
            this.currencyButton.Text = "Currency exchange";
            this.currencyButton.UseVisualStyleBackColor = true;
            // 
            // operationButton
            // 
            this.operationButton.Location = new System.Drawing.Point(442, 160);
            this.operationButton.Name = "operationButton";
            this.operationButton.Size = new System.Drawing.Size(75, 23);
            this.operationButton.TabIndex = 12;
            this.operationButton.Text = "Operation with currency";
            this.operationButton.UseVisualStyleBackColor = true;
            this.operationButton.Click += new System.EventHandler(this.operationButton_Click);
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(784, 225);
            this.Controls.Add(this.operationButton);
            this.Controls.Add(this.currencyButton);
            this.Controls.Add(this.frontLable);
            this.Controls.Add(this.pictureBox);
            this.Name = "MenuForm";
            this.Text = "start";
            this.Load += new System.EventHandler(this.MenuForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label frontLable;
        private System.Windows.Forms.Button currencyButton;
        private System.Windows.Forms.Button operationButton;
    }
}

