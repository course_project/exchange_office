﻿namespace exchange_office
{
    partial class Form2
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.saledCurrencyTextBox = new System.Windows.Forms.TextBox();
            this.boughtCurrencyTextBox = new System.Windows.Forms.TextBox();
            this.boughtCurrencyCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.SaledCurrencyCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.nextButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-2, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(780, 73);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Info;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(279, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(250, 36);
            this.label6.TabIndex = 11;
            this.label6.Text = "Enter informations";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label5.Location = new System.Drawing.Point(520, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "Step 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(279, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(235, 33);
            this.label4.TabIndex = 12;
            this.label4.Text = "Operation selection";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(232, 134);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(126, 17);
            this.radioButton1.TabIndex = 14;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Purchase of currency";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(453, 134);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(102, 17);
            this.radioButton2.TabIndex = 15;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Sale of currency";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // saledCurrencyTextBox
            // 
            this.saledCurrencyTextBox.Location = new System.Drawing.Point(55, 271);
            this.saledCurrencyTextBox.Multiline = true;
            this.saledCurrencyTextBox.Name = "saledCurrencyTextBox";
            this.saledCurrencyTextBox.Size = new System.Drawing.Size(168, 20);
            this.saledCurrencyTextBox.TabIndex = 20;
            this.saledCurrencyTextBox.Text = "Enter sale currency";
            // 
            // boughtCurrencyTextBox
            // 
            this.boughtCurrencyTextBox.Location = new System.Drawing.Point(55, 157);
            this.boughtCurrencyTextBox.Multiline = true;
            this.boughtCurrencyTextBox.Name = "boughtCurrencyTextBox";
            this.boughtCurrencyTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.boughtCurrencyTextBox.Size = new System.Drawing.Size(168, 20);
            this.boughtCurrencyTextBox.TabIndex = 18;
            this.boughtCurrencyTextBox.Text = "Enter buy currency";
            this.boughtCurrencyTextBox.TextChanged += new System.EventHandler(this.boughtCurrencyTextBox_TextChanged);
            // 
            // boughtCurrencyCheckedListBox
            // 
            this.boughtCurrencyCheckedListBox.FormattingEnabled = true;
            this.boughtCurrencyCheckedListBox.Items.AddRange(new object[] {
            "BYN",
            "EUR",
            "USD",
            "GBP"});
            this.boughtCurrencyCheckedListBox.Location = new System.Drawing.Point(55, 183);
            this.boughtCurrencyCheckedListBox.Name = "boughtCurrencyCheckedListBox";
            this.boughtCurrencyCheckedListBox.Size = new System.Drawing.Size(66, 49);
            this.boughtCurrencyCheckedListBox.TabIndex = 21;
            this.boughtCurrencyCheckedListBox.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // SaledCurrencyCheckedListBox
            // 
            this.SaledCurrencyCheckedListBox.FormattingEnabled = true;
            this.SaledCurrencyCheckedListBox.Items.AddRange(new object[] {
            "BYN",
            "EUR",
            "USD",
            "GBP"});
            this.SaledCurrencyCheckedListBox.Location = new System.Drawing.Point(55, 297);
            this.SaledCurrencyCheckedListBox.Name = "SaledCurrencyCheckedListBox";
            this.SaledCurrencyCheckedListBox.Size = new System.Drawing.Size(66, 49);
            this.SaledCurrencyCheckedListBox.TabIndex = 22;
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(337, 338);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 23;
            this.nextButton.Text = "Next";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(779, 385);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.SaledCurrencyCheckedListBox);
            this.Controls.Add(this.boughtCurrencyCheckedListBox);
            this.Controls.Add(this.saledCurrencyTextBox);
            this.Controls.Add(this.boughtCurrencyTextBox);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form2";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.TextBox saledCurrencyTextBox;
        private System.Windows.Forms.TextBox boughtCurrencyTextBox;
        private System.Windows.Forms.CheckedListBox boughtCurrencyCheckedListBox;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.CheckedListBox SaledCurrencyCheckedListBox;
        private System.Windows.Forms.Button nextButton;
    }
}

