﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class Limits
    {
        private double limit;
        private string currency;
        private bool boughtSaledFlag;

        public Limits (string currency,bool flag)
        {
            this.currency = currency;
            this.boughtSaledFlag = flag;
        }
        public double Limit
        {
            get
            {
                return limit;
            }

            set
            {
                limit = value;
            }
        }

        public string Currency
        {
            get
            {
                return currency;
            }

            set
            {
                currency = value;
            }
        }

        public bool BoughtSaledFlag
        {
            get
            {
                return boughtSaledFlag;
            }

            set
            {
                boughtSaledFlag = value;
            }
        }
    }
}
