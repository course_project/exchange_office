﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modal
{
    public class Rate
    {
        private double rateOfCurrency;
        private string currency;
        private bool boughtSaledFlag;

        public Rate(string currency, bool flag)
        {
            this.Currency = currency;
            this.BoughtSaledFlag = flag;
        }

        public double RateOfCurrency { get; set; }
        public string Currency { get; set; }
        public bool BoughtSaledFlag { get; set; }
    }
}
